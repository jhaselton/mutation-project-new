using System;
using System.IO;
using System.Collections.Generic;
namespace MutationsHappen
{
	public class sequenceObject
	{
		string fileType;
		List<int> start = new List<int>();
		List<int> end = new List<int>();
		double randomMutationRate, transitionRate, transversionRate, deaminationRate;
		string sequenceName;
		string originalSequence;
		char[] mutatedSequence;
		char [] codingArray;
		char [] mutatedCodingArray;
		double[] setrandomMutationPositions, settransversionPositions, settransitionPositions, setdeaminationPositions;
		char [] sourceTranslation, mutantTranslation;
		int[] setAAMutations;
		bool shouldWeMutateTransBiases, shouldWeMutateDeamBias;
		public sequenceObject (string name, string seq)
		{
			sequenceName = name;
			originalSequence = seq;
		}
		public string getName() {
			return(sequenceName);
		}
		public string getSequence() {
			return(originalSequence);
		}
		public string getFileType() {
			return(fileType);
		}
		public char[] getMutatedSequence() {
			return(mutatedSequence);
		}
		public char[] getCodingArray() {
			return(codingArray);
		}

		public List<int> getStartList() {
			return(start);
		}
		public List<int> getEndList() {
			return(end);
		}
		public void setFileType(string filetype) {
			fileType = filetype;
		}
		public void setStartList(List<int> startList) {
			start = startList;
		}
		public void setEndList(List<int> endList) {
			end = endList;
		}
		public void setMutatedSequence(char[] mutatedSeq) {
			mutatedSequence = mutatedSeq;
		}
		public void setCodingArray(char[] array) {
			codingArray = array;
		}
		//Random Mutation Rate and Positions
		public void setRandomMutationRate(double mutationRate) {
			randomMutationRate = mutationRate;
		}
		public double getRandomMutationRate() {
			return(randomMutationRate);
		}
		public void setRandomMutationPositions(double[] randomMutationPositions){
			setrandomMutationPositions = randomMutationPositions;
		}
		public double[] getRandomMutationPositions(){
			return setrandomMutationPositions;
		}
		//Mutational Biases Rates and Positions
		public void setTransitionRate(double setTransitionRate){
			transitionRate=setTransitionRate;
			shouldWeMutateTransBiases = true;
		}
		public double getTransitionRate(){
			return transitionRate;
		}
		public void setTransversionRate(double setTransversionRate){
			transversionRate = setTransversionRate;
		}
		public double getTransversionRate(){
			return transversionRate;
		}
		public void setDeaminationRate(double setDeaminationRate){
			deaminationRate=setDeaminationRate;
			shouldWeMutateDeamBias = true;
		}
		public double getDeaminationRate(){
			return deaminationRate;
		}
		public void setTransversionPositions(double[] transversionPositions){
			settransversionPositions = transversionPositions;
		}
		public double[] getTransversionPositions(){
			return settransversionPositions;
		}
		public void setTransitionPositions(double[] transitionPositions){
			settransitionPositions = transitionPositions;
		}
		public double[] getTransitionPositions(){
			return settransitionPositions;
		}
		public void setDeaminationPositions(double[] deaminationPositions){
			setdeaminationPositions = deaminationPositions;	
		}
		public double[] getDeaminationPositions(){
			return setdeaminationPositions;
		}
		public bool mutateTransBiases(){
			return shouldWeMutateTransBiases;
		}
		public bool mutateDeamBiases(){
			return shouldWeMutateDeamBias;
		}

		//Translated Sequences and Amino Acid Mutation Positions
		public void setSourceTranslation(char[] sourceTranslatedSeq){
			sourceTranslation = sourceTranslatedSeq;
		}
		public void setMutantTranslation(char[] mutantTranslatedSeq){
			mutantTranslation = mutantTranslatedSeq;
		}
		public char[] getSourceTranslation(){
			return sourceTranslation;
		}
		public char[] getMutantTranslation(){
			return mutantTranslation;
		}
		public void setAAMutationPositions(int[] aaMutations){
			setAAMutations = aaMutations;
		}
		public int[] getAAMutationPostions(){
			return setAAMutations;
		}
		public char[] getCodingMutationArray(){
			return mutatedCodingArray;
		}
		public void setCodingMutationArray(char[] mutatedArray) {
			mutatedCodingArray = mutatedArray;
		}
	}
}


