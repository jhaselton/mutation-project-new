﻿using System;
using System.IO;
using System.Collections.Generic;
namespace MutationsHappen
{
	public class MutateSequence
	{

		//Instantiate the random generator.
		MersenneTwister randGen = new MersenneTwister();


		/**************************************Completely Random Mutations***************************************/	
		public char[] RandomMutations(double generations, char[] randommutantseq, double mutationspergeneration, ref List<sequenceObject> sequenceList, int listPosition)
		{	//Length of the source array and instantiate variables
			int length = randommutantseq.Length;
			int mutantbase, mutationposition, i;
			char originalletter;

			//The actual number of mutations made is the product of generations and mutations/genome/generation
			double numbermutations = generations*mutationspergeneration;

			//An array that will store the position in the sequence where each mutation occurs.
			double[] storepositions = new double[Convert.ToInt32(numbermutations)];

			//Mutate the sequence for #generations
			for(i = 0; i < numbermutations; i++)
			{
				//Create a random number >= 0 and less than the length of the source/target array. This will be the mutation position.
				mutationposition = randGen.Next(length-1);
				//We may want to use this mutationpostion array in order to check and see if the spot has already been mutated, and if it has then
				//we could choose a different spot by generating another random number.
				storepositions[i] = mutationposition;


				//Create a random number between 0-3 (mutantbase) to determine what nucleotide base the random position will be mutated to.
				//While the mutation spot remains unchanged, generate a random number to mutate the position; we may
				//or may not want to ensure that it is changed in this way - we might want to add a chance for the position to be, say, fixed?
				//
				originalletter = randommutantseq[mutationposition];
				while(randommutantseq[mutationposition] == originalletter)
				{
					mutantbase = randGen.Next(3);
					if(mutantbase == 0){
						randommutantseq[mutationposition] = 'A';
					}
					if(mutantbase == 1){
						randommutantseq[mutationposition] = 'C';
					}
					if(mutantbase == 2){
						randommutantseq[mutationposition] = 'G';
					}
					if(mutantbase == 3){
						randommutantseq[mutationposition] = 'T';
					}
				}
			}
			//Sort the mutation positions and set them in the sequenceObject so that the output file will contain them in order.
			Array.Sort (storepositions);
			sequenceList[listPosition].setRandomMutationPositions(storepositions);
			Console.WriteLine ("\nThe total number of random mutations that occurred due to replication error is: " + numbermutations + "\n");
			return randommutantseq;
		}
		public char[] CodingMutations(double generations, char[] randommutantseq, double mutationspergeneration, ref List<sequenceObject> sequenceList, int listPosition)
		{	//Length of the source array and instantiate variables
			int length = randommutantseq.Length;
			int mutantbase, mutationposition, i;
			char originalletter;

			//The actual number of mutations made is the product of generations and mutations/genome/generation
			double numbermutations = generations*mutationspergeneration;

			//An array that will store the position in the sequence where each mutation occurs.
			double[] storepositions = new double[Convert.ToInt32(numbermutations)];

			//Mutate the sequence for #generations
			for(i = 0; i < numbermutations; i++)
			{

				do {
					//Create a random number >= 0 and less than the length of the source/target array. This will be the mutation position.
					mutationposition = randGen.Next(length-1);
					//We may want to use this mutationpostion array in order to check and see if the spot has already been mutated, and if it has then
					//we could choose a different spot by generating another random number.
				}while(randommutantseq[mutationposition] != ' ');
				storepositions[i] = mutationposition;

				//Create a random number between 0-3 (mutantbase) to determine what nucleotide base the random position will be mutated to.
				//While the mutation spot remains unchanged, generate a random number to mutate the position; we may
				//or may not want to ensure that it is changed in this way - we might want to add a chance for the position to be, say, fixed?
				//
				originalletter = randommutantseq[mutationposition];
				while(randommutantseq[mutationposition] == originalletter)
				{
					mutantbase = randGen.Next(3);
					if(mutantbase == 0){
						randommutantseq[mutationposition] = 'A';
					}
					if(mutantbase == 1){
						randommutantseq[mutationposition] = 'C';
					}
					if(mutantbase == 2){
						randommutantseq[mutationposition] = 'G';
					}
					if(mutantbase == 3){
						randommutantseq[mutationposition] = 'T';
					}
				}
			}

			sequenceList[listPosition].setRandomMutationPositions(storepositions);
			Console.WriteLine ("\nThe total number of random mutations that occurred due to replication error is: " + numbermutations + "\n");
			return randommutantseq;
		}

		//*****Obtains user input on random (replication error) mutation rates*****//
		public double ReplicationErrorRateLibrary(){
			double mutationrate, inputrate;
			bool doubleTest = false;

			Console.WriteLine ("For the rate of random and spontaneous mutations, would you like to: ");
			Console.WriteLine("1.Specify an organism.\n2.Enter an arbitrary mutation rate.\n3.Use a random rate between 0-100.\n");
			ConsoleKeyInfo choice;
			while (true) {
				choice = Console.ReadKey (true);
				if (choice.Key == ConsoleKey.D1) { 
					Console.WriteLine ("Choose from our library of organisms:\n 1.Human\n2.Virus");
					choice = Console.ReadKey (true);
					while (true) {
						choice = Console.ReadKey (true);
						if (choice.Key == ConsoleKey.D1) {
							//Mutations for humans are between 1-100 per generation.
							mutationrate = Convert.ToDouble (randGen.Next (100));
							Console.WriteLine ("The mutation rate is " + mutationrate + " per generation");
							return mutationrate;
						}
						if (choice.Key == ConsoleKey.D2) {
							//Virus
							mutationrate = 0.1;
							return mutationrate;
						}
					}
				}
				if (choice.Key == ConsoleKey.D2) {
					Console.WriteLine ("Enter a mutation rate (which is for the entire sequence/generation)");
					Console.WriteLine ("Please enter your rate as an integer or decimal value between 0.0000001 and 1000");
					doubleTest = double.TryParse(Console.ReadLine(), out inputrate);
					while (inputrate > 1000 || inputrate < 0.0000001 || !doubleTest) {
						Console.WriteLine ("You have entered an invalid rate. Please try again.");
						doubleTest = double.TryParse(Console.ReadLine(), out inputrate);
					}
					return inputrate;
				}
				if (choice.Key == ConsoleKey.D3) {
					mutationrate = Convert.ToDouble (randGen.Next (100));
					Console.WriteLine ("Your random mutation rate will be ");
					Console.Write (mutationrate);
					return mutationrate;
				}
			}
		}
		//*****Obtains Mutational biases mutation rates*****//
		public bool MutationalBiasesRateLibrary(ref List<sequenceObject> sequenceList, int listPosition){
			double transitionrate, transversionrate, deaminationrate;
			string response;

			Console.WriteLine("Would you like to enter rates for particular mutational biases?(Y/N)");
			ConsoleKeyInfo choice;
			choice = Console.ReadKey (true);
			if (choice.Key == ConsoleKey.Y) 
			{
				Console.WriteLine("Y");
				Console.WriteLine ("Would you like to enter transition/transversion rates?(Y/N)");
				choice = Console.ReadKey (true);
				if (choice.Key == ConsoleKey.Y)
				{
					Console.WriteLine("Y");
					//Get the rates from the user
					Console.WriteLine ("What transition mutation rate would you like to have per 100 nucleotides per generation? (Enter an integer or decimal value between 0.0000001 and 1000)");
					transitionrate = Convert.ToDouble(Console.ReadLine());
					while(transitionrate > 1000 || transitionrate < 0.0000001){
						Console.WriteLine ("You have entered an invalid rate. Please try again.");
						transitionrate = Convert.ToDouble(Console.ReadLine ());
					}
					sequenceList[listPosition].setTransitionRate(transitionrate);
					Console.WriteLine ("What transversion mutation rate would you like to have per 100 nucleotides per generation? (Enter an integer or decimal value between 0.0000001 and 1000)");
					transversionrate = Convert.ToDouble(Console.ReadLine());
					while(transversionrate > 1000 || transversionrate < 0.0000001){
						Console.WriteLine ("You have entered an invalid rate. Please try again.");
						transversionrate = Convert.ToDouble(Console.ReadLine ());
					}
					sequenceList[listPosition].setTransversionRate(transversionrate);
					Console.WriteLine ("Your transition rate is " + transitionrate + " and your transversion rate is " + transversionrate + "\n\n");
				}
				Console.WriteLine ("Would you like to enter a deamination mutation (C->T) rate?(Y/N)");
				choice = Console.ReadKey (true);
				if (choice.Key == ConsoleKey.Y)
				{
					Console.WriteLine("Y");
					Console.WriteLine ("Please keep in mind that rates should be very small, < 0.1");
					Console.WriteLine ("What deamination mutation rate would you like to have per 100 nucleotides per generation? (Enter an integer or decimal value between 0.00000001 and 1000)");
					response = Console.ReadLine();
					deaminationrate = Convert.ToDouble(response);
					while(deaminationrate > 100 || deaminationrate < 0.000000001){
						Console.WriteLine ("You have entered an invalid rate. Please try again.");
						deaminationrate = Convert.ToDouble(Console.ReadLine ());
					}
					sequenceList[listPosition].setDeaminationRate(deaminationrate);
				}
				return true;
			}
			else 
				return false;
		}

		//Object to obtain user-specified mutational bias information and mutate sequence according to each bias mutation rate.
		public char[] MutationalBiases(char[] sourceseq, double generations, ref List<sequenceObject> sequenceList, int listPosition)
		{
			double transitionrate, transversionrate, deaminationrate, numbertransitions, numbertransversions, numberdeaminations;
			int mutationposition, i, choosebase;
			double[] storetransitionpositions, storetransversionpositions, storedeaminationpositions;
			ConsoleKeyInfo choice;


			//Get the sequence length and divide by 100, which will be used to apply mutations for rates per 100 bases
			double doublelength = Convert.ToDouble(sourceseq.Length);
			double seqdividedby100 = Math.Ceiling (doublelength/100);

			int intlength = sourceseq.Length;
			char[] mutatedseq = new char[intlength];
			mutatedseq = sourceseq;

			//************************Transition/Transvesion Mutations************************//
			if(sequenceList[listPosition].mutateTransBiases() == true)
			{
				//Actual # of mutations = rate*generations*(seq/100)
				transitionrate = sequenceList[listPosition].getTransitionRate();
				transversionrate = sequenceList[listPosition].getTransversionRate();
				numbertransitions = Math.Ceiling(transitionrate*generations*seqdividedby100);
				numbertransversions = Math.Ceiling(transversionrate*generations*seqdividedby100);
				storetransitionpositions = new double[Convert.ToInt32(numbertransitions)];
				storetransversionpositions = new double[Convert.ToInt32(numbertransversions)];

				//*****Mutate the sequence for #transition mutations*****//
				for(i = 0; i < numbertransitions; i++)
				{
					//Create a random number >= 0 and less than the length of the source/target array. This will be the mutation position.
					mutationposition = randGen.Next(intlength-1);
					storetransitionpositions[i] = mutationposition;
					//Switch purines: A <-> G
					if(mutatedseq[mutationposition] == 'A')
						mutatedseq[mutationposition] = 'G';
					if(mutatedseq[mutationposition] == 'G')
						mutatedseq[mutationposition] = 'A';
					//Switch pyrimidines: C <-> T
					if(mutatedseq[mutationposition] == 'C')
						mutatedseq[mutationposition] = 'T';
					if(mutatedseq[mutationposition] == 'T')
						mutatedseq[mutationposition] = 'C';
				}
				//*****Mutate the sequence for #transversion mutations*****//
				for(i = 0; i < numbertransversions; i++)
				{
					//Create a random number >= 0 and less than the length of the source/target array. This will be the mutation position.
					mutationposition = randGen.Next(intlength-1);
					storetransversionpositions[i] = mutationposition;
					//Switch purines <-> pyrimidines by randomly selecting 0 or 1 to randomly decide between the two possible mutation choices
					if(mutatedseq[mutationposition] == 'A'){
						choosebase = randGen.Next (1);
						if(choosebase == 0)
							mutatedseq[mutationposition] = 'C';
						if(choosebase == 1)
							mutatedseq[mutationposition] = 'T';
					}
					if(mutatedseq[mutationposition] == 'C'){
						choosebase = randGen.Next (1);
						if(choosebase == 0)
							mutatedseq[mutationposition] = 'A';
						if(choosebase == 1)
							mutatedseq[mutationposition] = 'G';
					}
					if(mutatedseq[mutationposition] == 'G'){
						choosebase = randGen.Next (1);
						if(choosebase == 0)
							mutatedseq[mutationposition] = 'C';
						if(choosebase == 1)
							mutatedseq[mutationposition] = 'T';
					}
					if(mutatedseq[mutationposition] == 'T'){
						choosebase = randGen.Next (1);
						if(choosebase == 0)
							mutatedseq[mutationposition] = 'A';
						if(choosebase == 1)
							mutatedseq[mutationposition] = 'G';
					}

				}
				Console.WriteLine("The number of transitions that occurred is:");
				Console.Write (numbertransitions);
				Console.WriteLine (" ");
				Console.WriteLine("The number of transversions that occurred is:");
				Console.Write (numbertransversions);
				Console.WriteLine (" ");

				//Sort the mutation positions and set them in the sequenceObject so that the output file will contain them in order.
				Array.Sort(storetransitionpositions);
				Array.Sort (storetransversionpositions);
				sequenceList[listPosition].setTransversionPositions(storetransversionpositions);
				sequenceList[listPosition].setTransitionPositions(storetransitionpositions);
			}
			// C->T (Deamination) Mutation Bias
			if(sequenceList[listPosition].mutateDeamBiases() == true)
			{
				//The total number of deaminations = rate*generations*# 100 base pairs in seq
				deaminationrate = sequenceList[listPosition].getDeaminationRate();
				numberdeaminations = Math.Ceiling(deaminationrate*generations*seqdividedby100);
				storedeaminationpositions = new double[Convert.ToInt32(numberdeaminations)];

				for(i = 0; i < numberdeaminations; i++)
				{
					//Create a random number >= 0 and less than the length of the source/target array. This will be the mutation position.
					//Find a new position until the base at the position is a 'C', and then mutate this to a 'T'.
					mutationposition = randGen.Next(intlength-1);
					while(mutatedseq[mutationposition] != 'C'){
						mutationposition = randGen.Next(intlength-1);
					}
					if(mutatedseq[mutationposition] == 'C'){
						mutatedseq[mutationposition] = 'T';
						storedeaminationpositions[i] = mutationposition;
					}
				}
				Console.WriteLine("Number of deaminations: ");
				Console.Write (numberdeaminations);
				Console.WriteLine ();
				Array.Sort (storedeaminationpositions);
				sequenceList[listPosition].setDeaminationPositions(storedeaminationpositions);
			}
			return mutatedseq;
		}

		//***Find and Mutate CpG Islands****************************************************************************************//	
		//**********************************************************************************************************************//
		public int[,] FindCpg(char[] sourcesequence, double generations)
		{


			int length = sourcesequence.Length;
			int[] numbersequence = new int[length];
			int i, islandstart = 0, numberislands=0;
			double counter = 0, islandlength = 0;
			int[,] storecpgislands = new int[1000,2]; //Assuming there aren't more than 1000 islands... The 2D array contains [islandStartPosition, islandLength]
			char[] mutatedseq = new char[length]; 
			mutatedseq=sourcesequence;

			ConsoleKeyInfo choice;
			string response;

			//Variables to expand and look at the sequence outside of the found CG by +-2, 20, and 100 (thus looking at 4, 40 and 200 outside)
			//int expandtwo = 2,expandtwenty = 20, expandtwohundred = 100;



			for(i = 0;i < length; i++){
				if(sourcesequence[i] == 'A')
					numbersequence[i] = 0;
				if(sourcesequence[i] == 'T')
					numbersequence[i] = 0;
				if(sourcesequence[i] == 'C')
					numbersequence[i] = 1;
				if(sourcesequence[i] == 'G')
					numbersequence[i] = 2;
			}

			//If a C is found, and the next position is a G, then incrementally look outside of it to see if the surrounding region is >=50% GC Content
			int size = sourcesequence.Length;
			for(islandstart=0;islandstart<length-200;islandstart++){
				if (islandstart+2 < size) {
					if((sourcesequence[islandstart] == 'C') && (sourcesequence[islandstart+1] == 'G')){
						counter = 1;
						islandlength = 2;
						Console.Write(Math.Ceiling(counter/islandlength));
						while((Math.Ceiling(counter/islandlength) >= 0.5) && (islandstart+islandlength) < size){
							if (islandstart +Convert.ToInt32(islandlength)+2 < size) {
								if((sourcesequence[islandstart+Convert.ToInt32(islandlength)] == 'C') && (sourcesequence[islandstart+Convert.ToInt32(islandlength)+1] == 'G')){
									counter++;
									Console.WriteLine("Found " + counter + " CpGs");
								}

								islandlength++;

								Console.Write("Here");
							}
							islandlength++;
						}
						if(islandlength >= 200){
							storecpgislands[numberislands,0] = islandstart;
							storecpgislands[numberislands,1] = Convert.ToInt32(islandlength);
							numberislands++;
							islandstart++;
						}
						//islandlength++;

						/*		halfislandlength = 1;
					counter = 3;
					while((counter >= Math.Ceiling((1.5*halfislandlength))) && islandmiddle+halfislandlength<length && islandmiddle<length-100)
					{
						
						counter += numbersequence[islandmiddle-halfislandlength];
						counter += numbersequence[islandmiddle+halfislandlength];
						halfislandlength++;
						if(islandmiddle-halfislandlength < 0)
							islandmiddle++;
					}
					if(halfislandlength*2 >= 200)
					{
						storecpgislands[numberislands,0] = islandmiddle;
						storecpgislands[numberislands,1] = halfislandlength*2;
						numberislands++;
						islandmiddle += halfislandlength*2;
					}
			*/		
					}
				}
				Console.Write (islandstart);
			}
			Console.WriteLine ("The number of CpG Islands is:" + numberislands);
			Console.WriteLine("CpG Islands occur at positions:");
			for(i = 0; i < numberislands ; i++){
				Console.Write (storecpgislands[i,0]);
				Console.WriteLine (" and it is " + storecpgislands[i,1] + " long.");}

			//***********************Mutate CpG Islands************************//



			int mutationposition;
			double cpgdeaminationrate, cpgseqdividedby100 = 2, cpgnumberdeaminations;
			int[] storecpgdeaminationpositions;
			char[] cpgisland;

			Console.WriteLine ("Would you like to enter a CpG Island deamination mutation (C->T) rate?(Y/N)");
			choice = Console.ReadKey (true);
			if (choice.Key == ConsoleKey.Y)
			{
				Console.WriteLine("Y");
				Console.WriteLine ("Please keep in mind that rates should be very small, as in likely < 0.1");
				Console.WriteLine ("What CpG deamination mutation rate would you like to have per 100 CpG island nucleotides per generation? (Enter an integer or decimal value between 0.00000001 and 100)");
				response = Console.ReadLine();
				cpgdeaminationrate = Convert.ToDouble(response);
				while(cpgdeaminationrate > 100 || cpgdeaminationrate < 0.0000001){
					Console.WriteLine ("You have entered an invalid rate. Please try again.");
					cpgdeaminationrate = Convert.ToDouble(Console.ReadLine ());
				}


				//The total number of CpG deaminations = rate*generations*# 100 CpG Island base pairs in seq
				cpgnumberdeaminations = Math.Ceiling(cpgdeaminationrate*generations*cpgseqdividedby100);
				storecpgdeaminationpositions = new int[Convert.ToInt32(cpgnumberdeaminations)+100];

				string seq; 
				string c = "C";

				//Array.Copy(mutatedseq, (storecpgislands[i,0]-(storecpgislands[i,1]/2)), cpgisland, 1, (storecpgislands[i,0]-(storecpgislands[i,1]/2)));

				for(i=0;i<cpgnumberdeaminations;i++)
				{


					//Generate the mutation position between the found middle CpG Island position +-100
					mutationposition = randGen.Next(storecpgislands[i,0]-(storecpgislands[i,1]/2), storecpgislands[i,0]+(storecpgislands[i,1]/2));
					cpgisland = new char[storecpgislands[i,1]+10];
					Array.Copy(mutatedseq, (storecpgislands[i,0]-(storecpgislands[i,1]/2)), cpgisland, 0, (storecpgislands[i,0]+(storecpgislands[i,1]/2)));

					seq = Convert.ToString(cpgisland);


					while((mutatedseq[mutationposition] != 'C') && seq.Contains(c)){
						mutationposition = randGen.Next(storecpgislands[i,0]-(storecpgislands[i,1]/2), storecpgislands[i,0]+(storecpgislands[i,1]/2));
					}
					if(!seq.Contains(c))
						Console.WriteLine("There are no more Cytosines to mutate on this island."); 
					if(mutatedseq[mutationposition] == 'C'){
						mutatedseq[mutationposition] = 'T';
						storecpgdeaminationpositions[i] = mutationposition;
					}
				}

				Console.WriteLine("Number of CpG deaminations: ");
				Console.Write (cpgnumberdeaminations);
				Console.WriteLine ("\nWould you like to see where the deaminations occurred?(Y/N)");
				choice = Console.ReadKey (true);
				if (choice.Key == ConsoleKey.Y){
					Console.WriteLine ("Y");
					Console.WriteLine("CpG deaminations occurred at positions:");
					for(i = 0; i < cpgnumberdeaminations; i++){
						Console.Write (storecpgdeaminationpositions[i]+1);
						Console.WriteLine (" ");}
				}	
			}
			else if(choice.Key == ConsoleKey.N){
				return storecpgislands;
			}


			return storecpgislands;
		}


		public char get_amino_acid_from_codon(int a, int b, int c)
		{
			//Used by the arrayposition object to translate sets of three nucleotides (codons)
			List<char> codonsequence = new List<char>();	

			if (a==0 && b==0 && c==0)
				codonsequence.Add('T');
			else if(a==0 && b==0 && c==1)
				codonsequence.Add('N');
			else if(a==0 && b==0 && c==2)
				codonsequence.Add('N');
			else if(a==0 && b==0 && c==3)
				codonsequence.Add('K');
			else if(a==0 && b==1 && c==0)
				codonsequence.Add('I');
			else if(a==0 && b==1 && c==1)
				codonsequence.Add('I');
			else if(a==0 && b==1 && c==2)
				codonsequence.Add('I');
			else if(a==0 && b==1 && c==3)
				codonsequence.Add('M');
			else if(a==0 && b==2 && c==0)
				codonsequence.Add('T');
			else if(a==0 && b==2 && c==1)
				codonsequence.Add('T');
			else if(a==0 && b==2 && c==2)
				codonsequence.Add('T');
			else if(a==0 && b==2 && c==3)
				codonsequence.Add('T');
			else if(a==0 && b==3 && c==0)
				codonsequence.Add('R');
			else if(a==0 && b==3 && c==1)
				codonsequence.Add('S');
			else if(a==0 && b==3 && c==2)
				codonsequence.Add('S');
			else if(a==0 && b==3 && c==3)
				codonsequence.Add('R');
			else if(a==1 && b==0 && c==0)
				codonsequence.Add('X');
			else if(a==1 && b==0 && c==1)
				codonsequence.Add('Y');
			else if(a==1 && b==0 && c==2)
				codonsequence.Add('Y');
			else if(a==1 && b==0 && c==3)
				codonsequence.Add('X');
			else if(a==1 && b==1 && c==0)
				codonsequence.Add('L');
			else if(a==1 && b==1 && c==1)
				codonsequence.Add('F');
			else if(a==1 && b==1 && c==2)
				codonsequence.Add('F');
			else if(a==1 && b==1 && c==3)
				codonsequence.Add('L');
			else if(a==1 && b==2 && c==0)
				codonsequence.Add('S');
			else if(a==1 && b==2&& c==1)
				codonsequence.Add('S');
			else if(a==1 && b==2 && c==2)
				codonsequence.Add('S');
			else if(a==1 && b==2 && c==3)
				codonsequence.Add('S');
			else if(a==1 && b==3 && c==0)
				codonsequence.Add('X');
			else if(a==1 && b==3 && c==1)
				codonsequence.Add('C');
			else if(a==1 && b==3 && c==2)
				codonsequence.Add('C');
			else if(a==1 && b==3 && c==3)
				codonsequence.Add('W');
			else if(a==2 && b==0 && c==0)
				codonsequence.Add('Q');
			else if(a==2 && b==0 && c==1)
				codonsequence.Add('H');
			else if(a==2 && b==0 && c==2)
				codonsequence.Add('H');
			else if(a==2 && b==0 && c==3)
				codonsequence.Add('Q');
			else if(a==2 && b==1 && c==0)
				codonsequence.Add('L');
			else if(a==2 && b==1 && c==1)
				codonsequence.Add('L');
			else if(a==2 && b==1 && c==2)
				codonsequence.Add('L');
			else if(a==2 && b==1 && c==3)
				codonsequence.Add('L');
			else if(a==2 && b==2 && c==0)
				codonsequence.Add('P');
			else if(a==2 && b==2 && c==1)
				codonsequence.Add('P');
			else if(a==2 && b==2 && c==2)
				codonsequence.Add('P');
			else if(a==2 && b==2 && c==3)
				codonsequence.Add('P');
			else if(a==2 && b==3 && c==0)
				codonsequence.Add('R');
			else if(a==2 && b==3 && c==1)
				codonsequence.Add('R');
			else if(a==2 && b==3 && c==2)
				codonsequence.Add('R');
			else if(a==2 && b==3 && c==3)
				codonsequence.Add('R');
			else if(a==3 && b==0 && c==0)
				codonsequence.Add('E');
			else if(a==3 && b==0 && c==1)
				codonsequence.Add('D');
			else if(a==3 && b==0 && c==2)
				codonsequence.Add('D');
			else if(a==3 && b==0 && c==3)
				codonsequence.Add('E');
			else if(a==3 && b==1 && c==0)
				codonsequence.Add('V');
			else if(a==3 && b==1 && c==1)
				codonsequence.Add('V');
			else if(a==3 && b==1 && c==2)
				codonsequence.Add('V');
			else if(a==3 && b==1 && c==3)
				codonsequence.Add('V');
			else if(a==3 && b==2 && c==0)
				codonsequence.Add('A');
			else if(a==3 && b==2 && c==1)
				codonsequence.Add('A');
			else if(a==3 && b==2 && c==2)
				codonsequence.Add('A');
			else if(a==3 && b==2 && c==3)
				codonsequence.Add('A');
			else if(a==3 && b==3 && c==0)
				codonsequence.Add('G');
			else if(a==3 && b==3 && c==1)
				codonsequence.Add('G');
			else if(a==3 && b==3 && c==2)
				codonsequence.Add('G');
			else if(a==3 && b==3 && c==3)
				codonsequence.Add('G');

			char[] codonsequencearray = codonsequence.ToArray();
			//Console.WriteLine(codonsequencearray[0]);
			return codonsequencearray[0];
		}
	}
}

