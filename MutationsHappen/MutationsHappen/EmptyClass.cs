﻿using System;

namespace Application
{
	public static void Main()
	{
		struct Node;

		ifstream in;
		char * GBK_file = "mm_ref_GRCm38.p2_chr1.gbk";
		in.open(GBK_file);
		if (in.is_open()){
			char z[500];
			in.getline(z, 500, '\n');
			string tkn1 = "gene            complement";
			string tkn2 = "..";

			while(in.peek()!=EOF ){
				string str = z; // complier work aroundddd
				size_t found = str.find(tkn1);
				if (found!=std::string::npos){ // if gene found

					found = str.find(tkn2); //find .. to indicate coordinates
					int j = found -1; // first number before ..
					int k = found +2; //first number after..

					char c = str[j];
					string rstart;
					while (c!='('){ // c is a number
						rstart+=c; //expand backwards from .. to get gene start
						j--;
						c = str[j];

					}
					string gene_start;


					for(int i=rstart.size()-1; i>=0; i--){
						char f = rstart[i];
						gene_start+=f; //reverse the string to get the right coord
					}
					c = str[k];
					string gene_end;
					while(c!=')'){
						gene_end+=c;
						k++;

						c=str[k];
					}

					in.getline(z, 500, '\n');
					str = z;
					string tkn3 = "gene=";
					found = str.find(tkn3);
					int u = found+6;
					c = str[u];
					string gene;
					while (c!='"'){
						gene += c;
						u++;
						c=str[u];

					}
					in.getline(z, 500, '\n');
					cout<<gene<<" "<<gene_start<<".."<<gene_end<<endl;


					// AT THIS POINT
					//gene_start = start coord
					//gene_end = end coord
					//gene = gene name
					//pass these on to whatever datastructure is needed!


				}
				else
					if (in.peek()!=EOF)
						in.getline(z,500,'\n');
			}
		}
	}
}

