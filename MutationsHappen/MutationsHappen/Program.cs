using System;
using System.IO;
using System.Collections.Generic;
using  System.Text.RegularExpressions;
namespace MutationsHappen
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string filename;
			int isEmpty = 0;
			bool doubleTest = false;
			double generations = 0;
			bool doBiases = false;
			char[] mutatedseq;
			List<sequenceObject> sequenceList = new List<sequenceObject> ();
			do {
				filename = getFile ();
				if (filename != null) {
					if (filename.Contains(".fasta")) {
						parseFasta (filename, ref sequenceList);
					}
					if (filename.Contains(".gb")) {
						parseGBK(filename, ref sequenceList);
					}
					if (!filename.Contains(".gb") && !filename.Contains(".fasta"))
					{
						Console.WriteLine("Please enter either a .gb file or a .fasta file.");
					}
					isEmpty = sequenceList.Count;
				}
			} while (isEmpty == 0);
			ConsoleKeyInfo fileChoice;
			Console.WriteLine ("Do you wish to enter another file? (Y/N)");
			do {
				while(true) {
					fileChoice = Console.ReadKey(true);
					if (fileChoice.Key == ConsoleKey.Y) {
						Console.WriteLine("Y");
						filename = getFile ();
						parseFasta (filename, ref sequenceList);
						Console.WriteLine("Do you wish to enter another file? (Y/N)");
					}
					if (fileChoice.Key == ConsoleKey.N) {
						Console.WriteLine("N");
						break;
					}
				}
			} while (fileChoice.Key != ConsoleKey.N);
			MutateSequence test = new MutateSequence ();
			while (!doubleTest) {
				Console.WriteLine ("How many generations would you like to test? (Enter an integer value)");
				doubleTest = double.TryParse (Console.ReadLine (), out generations);
				if (!doubleTest) {
					Console.WriteLine ("Please enter an integer.");
				}
			}
			int listSize = sequenceList.Count;
			for (int i = 0; i < listSize; i++) {
				if (sequenceList [i].getFileType () == "FASTA") {
					sequenceList [i].setCodingArray (getCoding (sequenceList[i], ref sequenceList, i));				
				}
				else 
				{
					if (sequenceList[i].getStartList().Count != 0)
					{
						sequenceList [i].setCodingArray (GBKCodingArray (sequenceList [i]));
						Console.WriteLine (sequenceList [i].getCodingArray ());	
					}
				}
			}
			for (int i = 0; i < listSize; i++) {
				mutatedseq = sequenceList[i].getSequence().ToCharArray();
				Console.WriteLine ("For " + sequenceList [i].getName ());
				Console.WriteLine("Do you want to randomly mutate the sequence? (Y/N)");
				ConsoleKeyInfo choice;
				choice = Console.ReadKey (true);
				if (choice.Key == ConsoleKey.Y){
					sequenceList[i].setRandomMutationRate(test.ReplicationErrorRateLibrary());
					sequenceList[i].setMutatedSequence(test.RandomMutations(generations, sequenceList[i].getSequence().ToCharArray(), sequenceList[i].getRandomMutationRate(), ref sequenceList, i));
					mutatedseq = sequenceList[i].getMutatedSequence();
				}
				doBiases = test.MutationalBiasesRateLibrary(ref sequenceList, i);
				if(doBiases == true){
					sequenceList[i].setMutatedSequence(test.MutationalBiases(mutatedseq,generations, ref sequenceList,i));
				}
			}
			/*for (int i = 0; i < listSize; i++) {
				//unifinished
				if (sequenceList [i].getStartList ().Count != 0) {
					Console.WriteLine ("Entering Coding mutation rate for " + sequenceList [i].getName ());
				//	sequenceList [i].setCodingMutationArray (test.ReplicationErrorRateLibrary ());
				}
			}
			for (int i = 0; i < listSize; i++) {
				sequenceList[i].setMutatedSequence(test.RandomMutations(generations, sequenceList[i].getSequence().ToCharArray(), sequenceList[i].getRandomMutationRate(), ref sequenceList, i));
					}
			for (int i = 0; i < listSize; i++) {
				//sequenceList [i].setCodingMutationArray (test.CodingMutations (generations, sequenceList [i].getCodingArray (), sequenceList [i].getCodingMutationRate (), ref sequenceList, i));
			}*/

			makeOutputFile (ref sequenceList);
			makeSecondOutputFile(ref sequenceList);



		}


		//Gets input file name
		public static string getFile() {
			Console.Write ("Enter the name of a FASTA or GBK file: ");
			string path = Directory.GetCurrentDirectory();
			string SequenceFile = path + "/" + Console.ReadLine ();
			if (!File.Exists (SequenceFile)) {
				Console.Write ("File not found, please try again.\n");
				return null;
			}
			return (SequenceFile);
		}
		//Parses sequence FASTA file into program
		public static void parseFasta(string file, ref List<sequenceObject> sequenceList) {
			string name;
			string seq = "";
			FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);
			using (StreamReader sr = new StreamReader (stream)) {
				while (!sr.EndOfStream) {
					if (sr.Peek () != 62) {
						Console.WriteLine ("File is not in FASTA format, please enter a FASTA file.");
						break;
					}
					name = sr.ReadLine ();
					while ((sr.Peek () != 62) && !(sr.EndOfStream)) {
						seq += sr.ReadLine ();
					}
					seq = seq.ToUpper();
					foreach (char element in seq) {
						if (element != 'A' && element != 'C' && element != 'T' && element != 'G') {
							Console.WriteLine ("An error has occurred, not all elements in the sequence are A T C or G. \n Please enter a valid sequence.");
							return;
						}
					}
					sequenceObject sequence = new sequenceObject (name, seq);
					sequence.setFileType ("FASTA");
					sequenceList.Add (sequence);
					seq = "";
				}
			}
		}
		public static void parseGBK(string file, ref List<sequenceObject> sequenceList) {
			string name = "";
			string seq = "";
			string line;
			string line2;
			string[] split;
			char[] delim = {' ','\t'};
			List<int> start = new List<int>();
			List<int> end = new List<int>();
			FileStream stream = new FileStream (file, FileMode.Open, FileAccess.Read);
			using (StreamReader sr = new StreamReader (stream)) {
				while (!sr.EndOfStream) {
					line = sr.ReadLine ();
					line2 = line.Trim ();
					split = line2.Split (delim);
					switch (split [0]) {
					case "DEFINITION":
						name = line.Substring (10);
						break;
					case "exon":
						string temp = split [12];
						string[] temp2 = temp.Split ('.');
						start.Add (Int32.Parse (temp2 [0]));
						end.Add (Int32.Parse (temp2 [2]));
						break;
					case "ORIGIN":
						string text = sr.ReadToEnd ();
						string[] split2 = text.Split (' ');
						List<char> tempList = new List<char> ();
						foreach (string s in split2) {
							char[] charsplit = s.ToCharArray ();
							foreach (char t in charsplit) {
								if (t.Equals ('a') || t.Equals ('t') || t.Equals ('c') || t.Equals ('g')) {
									tempList.Add (t);
								}
							}
						}
						char[] temparray = tempList.ToArray ();
						seq = string.Join ("", temparray);
						break;
					}
				}
				name = ">"+name.Trim ();
				seq = seq.ToUpper ();
				sequenceObject sequence = new sequenceObject (name, seq);
				sequence.setStartList (start);
				sequence.setEndList (end);
				sequence.setFileType ("GB");
				sequenceList.Add (sequence);
			}
		}
		public static Array makeFull(string seq) {
			//
			// Instantiate the source array.
			//
			char[] realsource = seq.ToCharArray();
			//
			return realsource;
		}
		public static char[] getCoding(sequenceObject seq, ref List<sequenceObject> sequenceList, int listposition) {
			//
			// Instantiate the source array.
			//
			List<int> temp = new List<int>();
			List<int> temp2 = new List<int>();
			char[] source = seq.getSequence().ToCharArray();
			//
			//Length of the source array
			//
			int length = source.Length;
			//
			// Instantiate and allocate the target array.
			//
			char[] target = new char[length];
			//
			//Coding region loop
			//
			Console.WriteLine ("Do you have any coding regions for " +seq.getName() + "?(Y/N)");
			ConsoleKeyInfo choice;
			do {
				while (true) {
					choice = Console.ReadKey (true);
					if (choice.Key == ConsoleKey.Y) 
					{ 
						Console.WriteLine("Y");
						//
						// Ask for start of coding region
						//
						int codingstart = 1;
						bool intTest = false;
						while (!intTest || codingstart>=length || codingstart <0) {
							Console.WriteLine ("At what position does your coding region start?");
							intTest = int.TryParse(Console.ReadLine(), out codingstart);
							codingstart--;
							if (!intTest || codingstart>=length || codingstart <0) {
								Console.WriteLine("Input is not an integer or not within the length of the sequence, please try again.");
							}
						}
						temp.Add(codingstart+1);
						sequenceList[listposition].setStartList(temp);
						//
						// Ask for end of coding region
						//
						int codingend = 1;
						intTest = false;
						while (!intTest || codingend>length || codingend <0 || codingend<=codingstart) {
							Console.WriteLine ("At what position does your coding region end?");
							intTest = int.TryParse(Console.ReadLine(), out codingend);
							codingend--;
							if (!intTest || codingend>length || codingend <0 || codingend<=codingstart) {
								Console.WriteLine("Input is not an integer or not within the length of the sequence, please try again.");
							}
						}
						temp2.Add(codingend+1);
						sequenceList[listposition].setEndList(temp2);
						int realcodingend = codingend - codingstart + 1;
						//
						//
						// Copy the source to the target.
						//
						Array.Copy (source, codingstart, target, codingstart, realcodingend);
						//
						//
						//
						Console.WriteLine ("Do you have any more coding regions?(Y/N)");
						break;
					} 
					else if (choice.Key == ConsoleKey.N)
					{
						Console.WriteLine("N");
						break;
					} 
				}
			} while (choice.Key != ConsoleKey.N);
			return target; 
		}
		public static char[] GBKCodingArray(sequenceObject seq) {
			List<int> temp1 = seq.getStartList ();
			List<int> temp2 = seq.getEndList ();
			char[] source = seq.getSequence().ToCharArray();
			int listLength = temp1.Count;
			int length = source.Length;
			int codingstart = 0;
			int codingend = 0;
			char[] target = new char[length];
			for (int i = 0; i < listLength; i++) {
				codingstart = temp1[i] - 1;
				codingend = temp2 [i] - 1;
				int realcodingend = codingend - codingstart + 1;
				Array.Copy (source, codingstart, target, codingstart, realcodingend);
			}
			return target;
		}

		//Returns a translated amino acid sequence
		public static char[] arrayposition(char[] fullsequence)
		{
			int one = 0;
			int two = 0;
			int three = 0;
			char test;
			List<char> translatedSequence = new List<char>();
			MutateSequence aminoacids = new MutateSequence();
			for (int i = 0; i < (fullsequence.Length-2); i+=3)
			{
				char first = fullsequence[i];
				char second = fullsequence[i + 1];
				char third = fullsequence[i + 2];
				char A = 'A';
				int a = 0;
				char T = 'T';
				int t = 1;
				char C = 'C';
				int c = 2;
				char G = 'G';
				int g = 3;

				if(first.Equals(A))
					one = a;
				else if(first.Equals(T))
					one = t;
				else if(first.Equals(C))
					one = c;
				else if(first.Equals(G))
					one = g;

				if(second.Equals(A))
					two = a;
				else if(second.Equals(T))
					two = t;
				else if(second.Equals(C))
					two = c;
				else if(second.Equals(G))
					two = g;

				if(third.Equals(A))
					three = a;
				else if(third.Equals(T))
					three = t;
				else if(third.Equals(C))
					three = c;
				else if(third.Equals(G))
					three = g;

				test = aminoacids.get_amino_acid_from_codon(one,two,three);
				translatedSequence.Add(test);
			}		
			char [] translatedArray = translatedSequence.ToArray ();
			Console.Write(translatedArray);
			return translatedArray;
		}
		// Returns a amino acid for a 3 nucleotide input

		//Makes output FASTA file.
		public static void numberofnucelotides(char[] fullsequence)
		{
			int nucleotides = 0;
			for(int i = 0; i<= fullsequence.Length; i++)
			{
				if(fullsequence[i].Equals('A'))
				{
					nucleotides++;
					Console.WriteLine(fullsequence[i]);
				}
				else if(fullsequence[i].Equals('T'))
				{
					nucleotides++;
					Console.WriteLine(fullsequence[i]);
				}
				else if(fullsequence[i].Equals('C'))
				{
					nucleotides++;
					Console.WriteLine(fullsequence[i]);
				}
				else if(fullsequence[i].Equals('G'))
				{
					nucleotides++;
					Console.WriteLine(fullsequence[i]);
				}
				else if(fullsequence[i].Equals('U'))
				{
					nucleotides++;
					Console.WriteLine(fullsequence[i]);
				}
				Console.WriteLine(nucleotides);

			}

			deletespaces(nucleotides, fullsequence);

		}
		public static char[] deletespaces(int nucleotides, char[] fullsequence)
		{
			List<char> nospaces = new List<char>();
			foreach(char s in fullsequence)
			{
				nospaces.Add(s);
			}

			char[] nospacesarray = nospaces.ToArray();
			Console.WriteLine(nospacesarray);
			return nospacesarray;
		}
		public static List<int> where_are_mutations(char[] array1, char[] array2, int start, int end)
		{
			int length = array1.Length;
			//make empty array the same size as the full sequence array so that we can put markers anywhere there is a mutation
			char[] mutations = new char[length];
			//make a list to store the positions of where synonymous mutations occurred
			List<int> synonymous = new List<int>();
			//put 'A' anytime the two arrays differ i.e. where there's a mutation
			for(int i=0; i<length; i++)
			{
				if(array1[i] != array2[i])
				{
					mutations[i] = 'A';
				}

			}
			//inspect each mutation's position. If it is greater than the start and less than the end of the coding region, then it is synonymous.
			for(int k=0; k<length; k++)
			{
				if(mutations[k].Equals('A'))
				{
					Console.WriteLine("You have a mutation at " + k + " where " + array1[k] + " became " + array2[k]);
					if(k>=start && k<=end)
					{
						Console.WriteLine("...and it's synonymous!");
						Console.WriteLine("");
						synonymous.Add(k);
					}
				}
				//in case the arrays are the same
				if(array1 == array2)
				{
					Console.WriteLine("Wow, no mutations...nice!");
				}


			}
			//return a List<int> that contains the positions of each synonymous mutation.
			return synonymous;

		}
		public static void makeOutputFile(ref List<sequenceObject> list) {
			bool newline = false;
			string path = Directory.GetCurrentDirectory();
			int newFile = 1;
			string SequenceFile = path + "/mutatedSequence.fasta";
			while (File.Exists (SequenceFile)) {
				SequenceFile = path + "/mutatedSequence" + newFile.ToString() + ".fasta";
				newFile++;
			}

			using (System.IO.StreamWriter file = new System.IO.StreamWriter(SequenceFile))
			{
				int listSize = list.Count;
				for (int i = 0; i < listSize; i++) {
					file.WriteLine(list [i].getName ());
					char[] outputseq = (list [i].getMutatedSequence());
					int outputlength = outputseq.GetLength(0)-1;
					for(int j =0; j < outputlength; j++) {
						if ((j+1)%79 !=0) {
							file.Write(outputseq[j]);
							newline = true;
						}
						else
						{
							file.Write(outputseq[j]);		
							file.WriteLine();
							newline = false;
						}
					}
					if (newline) {
						file.WriteLine ();
					}
				}
				file.Close ();
			}
			Console.WriteLine ("Output file successfully made.");
		}
		public static void makeSecondOutputFile(ref List<sequenceObject> list) {
			bool newline = false;
			string path = Directory.GetCurrentDirectory();
			int newFile = 1;
			string SequenceFile = path + "/MutationInformation.txt";
			while (File.Exists (SequenceFile)) {
				SequenceFile = path + "/MutationInformation" + newFile.ToString() + ".txt";
				newFile++;
			}

			using (System.IO.StreamWriter file = new System.IO.StreamWriter(SequenceFile))
			{
				int listSize = list.Count;
				for (int i = 0; i < listSize; i++) {
					file.WriteLine("For " + list [i].getName ());
					file.WriteLine ();
					char[] mutantseq = (list [i].getMutatedSequence());
					char[] sourceseq = (list [i].getSequence().ToCharArray());
					char[] mutanttranslation = (list[i].getMutantTranslation());
					char[] sourcetranslation = (list[i].getSourceTranslation());
					int[] aaPositions = (list[i].getAAMutationPostions());
					double[] basePositions =(list[i].getRandomMutationPositions());
					int numberbasemutations = basePositions.GetLength(0);
					int numberAAmutations = aaPositions.GetLength(0);
					int mutantseqlength = mutantseq.GetLength(0)-1;
					file.WriteLine("Original Sequence: ");
					for(int j =0; j < mutantseqlength; j++) {
						if ((j+1)%79 !=0) {
							file.Write(sourceseq[j]);
							newline = true;
						}
						else
						{
							file.Write(sourceseq[j]);		
							file.WriteLine();
							newline = false;
						}
					}
					if (newline) {
						file.WriteLine ();
					}
					file.WriteLine ();
					file.WriteLine("Mutant Sequence: ");
					for(int j =0; j < mutantseqlength; j++) {
						if ((j+1)%79 !=0) {
							file.Write(mutantseq[j]);
							newline = true;
						}
						else
						{
							file.Write(mutantseq[j]);		
							file.WriteLine();
							newline = false;
						}
					}
					if (newline) {
						file.WriteLine ();
					}
					file.WriteLine ();	
					file.WriteLine("The " + numberbasemutations + " Spontaneous Nucleotide Mutations Occurred at Positions: ");
					for(int j =0; j < numberbasemutations; j++) {
						if(j != numberbasemutations-1){
							file.Write(basePositions[j]+", ");	
						}
						else{
							file.Write (basePositions[j]);
							j++;
						}
					}
					file.WriteLine ();
					file.WriteLine ();	
					//Check to see if transition mutations were made; if not, do not write anything about them to file.	
					if(!(list[i].getTransitionPositions() == null)){
						double[] transitionPositions =(list[i].getTransitionPositions());
						int numbertransitions = transitionPositions.GetLength(0);	
						file.WriteLine("The " + numbertransitions + " Transition Mutations Occurred at Positions: ");
						for(int j =0; j < numbertransitions; j++) {
							if(j != numbertransitions-1){
								file.Write(transitionPositions[j]+", ");	
							}
							else{
								file.Write (transitionPositions[j]);
								j++;
							}
						}
						file.WriteLine ();	
					}
					//Check to see if transversion mutations were made; if not, do not write anything about them to file.	
					if(!(list[i].getTransversionPositions() == null)){
						double[] transversionPositions =(list[i].getTransversionPositions());	
						int numbertransversions = transversionPositions.GetLength(0);
						file.WriteLine("The " + numbertransversions + " Transversion Mutations Occurred at Positions: ");
						for(int j =0; j < numbertransversions; j++) {
							if(j != numbertransversions-1){
								file.Write(transversionPositions[j]+", ");	
							}
							else{
								file.Write (transversionPositions[j]);
								j++;
							}
						}
						file.WriteLine ();
					}	
					//Check to see if deamination mutations were made; if not, do not write anything about them to file.	
					if(!(list[i].getDeaminationPositions() == null)){	
						double[] deaminationPositions =(list[i].getDeaminationPositions());
						int numberdeaminations = deaminationPositions.GetLength(0);		
						file.WriteLine("The " + numberdeaminations + " Deamaintion Mutations Occurred at Positions: ");
						for(int j =0; j < numberdeaminations; j++) {
							if(j != numberdeaminations-1){
								file.Write(deaminationPositions[j]+", ");	
							}
							else{
								file.Write (deaminationPositions[j]);
								j++;
							}
						}
						file.WriteLine ();	
					}			
					file.WriteLine ();		
					file.WriteLine("Translated Original Sequence: ");
					int translationlength = sourcetranslation.GetLength(0);
					for(int j =0; j < translationlength; j++) {
						if ((j+1)%79 !=0) {
							file.Write(sourcetranslation[j]);
							newline = true;
						}
						else
						{
							file.Write(sourcetranslation[j]);		
							file.WriteLine();
							newline = false;
						}
					}
					if (newline) {
						file.WriteLine ();
					}
					file.WriteLine ();	
					file.WriteLine("Translated Mutant Sequence: ");
					for(int j =0; j < translationlength; j++) {
						if ((j+1)%79 !=0) {
							file.Write(mutanttranslation[j]);
							newline = true;
						}
						else
						{
							file.Write(mutanttranslation[j]);		
							file.WriteLine();
							newline = false;
						}
					}
					if (newline) {
						file.WriteLine ();
					}
					file.WriteLine ();	

					file.WriteLine("Within the amino acid sequence, the " + numberAAmutations + " Non-Synonymous Mutations Occurred at Positions: ");
					for(int j =0; j < numberAAmutations; j++) {
						if(j != numberAAmutations-1){
							file.Write(aaPositions[j]+", ");	
						}
						else{
							file.Write (aaPositions[j]);
							j++;
						}
					}
					file.WriteLine ();
					file.WriteLine ();	
				}
				file.Close ();
			}
			Console.WriteLine ("Output file successfully made.");
		}
		// Combines coding region mutations and random mutation arrays
		public static char[] overwrite(char[] full, char[] coding) {

			int length = full.Length;
			int i;
			for(i = 0; i < length; i++)
			{
				if(coding[i] != ' ')
				{
					full[i] = coding[i];
				}
			}
			return full;
		}
	}
}
